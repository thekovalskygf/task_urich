﻿<?php
/**
 * Plugin Name:       Tour
 * Plugin URI:        https://example.com/plugins/the-basics/
 * Description:       Handle the basics with this plugin.
 * Version:           1.10.3
 * Requires at least: 5.2
 * Requires PHP:      7.3
 * Author:            Sergey Stus
 * Author URI:        https://author.example.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       tour
 * Domain Path:       /lang
 */

if( !defined('ABSPATH')) {
    die;
}
//
/******************************** Tour::create ********************************/
function submenu_create() {
    add_submenu_page(
        'tour-options',
        'Страница: все',
        'Новый тур',
        'manage_options',
        'submenu_create',
        'tour_item_create_content',
        'page_all_callback'
	);
}
add_action('admin_menu', 'submenu_create');

function tour_item_create_content() {
    echo '<div class="wrap"><h1>Новый тур</h1></div>';
}

/******************************** Tour::all ********************************/
function submenu_all() {
    add_submenu_page(
        'tour-options',
        'Страница: все',
        'Все туры',
        'manage_options',
        'create-submenu_create',
        'tour_all_content',
        'page_all_callback'
	);
}
add_action('admin_menu', 'submenu_all');

function tour_all_content() {
    echo '<div class="wrap"><h1>Все туры</h1></div>';
    
}

add_action( 'init', 'register_post_types' );
function register_post_types(){
	register_post_type( 'tours', [
		'label'  => null,
		'labels' => [
			'name'               => 'Туры', // основное название для типа записи
			'singular_name'      => 'Туры', // название для одной записи этого типа
			'add_new'            => 'Добавить тур', // для добавления новой записи
			'add_new_item'       => 'Добавление тура', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование тура', // для редактирования типа записи
			'new_item'           => 'Новое тур', // текст новой записи
			'view_item'          => 'Смотреть туры', // для просмотра записи этого типа.
			'search_items'       => 'Поиск туров', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => 'qw', // для родителей (у древовидных типов)
			'menu_name'          => 'Туры', // название меню
		],
		'description'         => 'Описание',
		'public'              => true,
		// 'publicly_queryable'  => null, // зависит от public
		// 'exclude_from_search' => null, // зависит от public
		// 'show_ui'             => null, // зависит от public
		// 'show_in_nav_menus'   => null, // зависит от public
		'show_in_menu'        => null, // показывать ли в меню адмнки
		// 'show_in_admin_bar'   => null, // зависит от show_in_menu
		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => null,
		'menu_icon'           => null,
		//'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => [ 'title', 'editor', 'thumbnail' ], // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => [],
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
		//'taxonomies'  => array( 'category' ),
	] );
}

// Регистрируем колонку 'ID' и 'Миниатюра'. Обязательно.
add_filter( 'manage_tours_posts_columns', function ( $columns ) {
	$my_columns = [
		'id'    => 'ID',
		'country' => 'Страна',
		'price' => 'Стоимость',
		'about' => 'Описание',
		'start_date' => 'Начало тура',
		'end_date' => 'Конец тура',
		'images' => 'Картинки',
	];

	return array_slice( $columns, 0, 1 ) + $my_columns + $columns;
} );

// Выводим контент для каждой из зарегистрированных нами колонок. Обязательно.
add_action( 'manage_tours_posts_custom_column', function ( $column_name ) {
	if ( $column_name === 'id' ) {
		echo the_ID();
	}
	if ( $column_name === 'price' ) {
		echo the_field('price');
	}
	if ( $column_name === 'country' ) {
		echo the_field('country');
	}
	if ( $column_name === 'about' ) {
		echo the_content();
	}
	if ( $column_name === 'start_date' ) {
		echo the_field('start_date');
	}
	if ( $column_name === 'end_date' ) {
		echo the_field('end_date');
	}
	if ( $column_name === 'images' ) {
		
		$imgurl = get_post_thumbnail_id($post->ID);
		$size = add_image_size( 'size', 55, 55, true );
		$sss = wp_get_attachment_image($imgurl, 'size');
		    echo $sss;	
	}

	if ( $column_name === 'thumb' && has_post_thumbnail() ) {
		?>
		<a href="<?php echo get_edit_post_link(); ?>">
			<?php the_post_thumbnail( 'thumbnail' ); ?>
		</a>
		<?php
	}
} );

// Добавляем стили для зарегистрированных колонок. Необязательно.
add_action( 'admin_print_footer_scripts-edit.php', function () {
	?>
	<style>
		.column-id {
			width: 60px;
		}
		.column-thumb img {
			max-width: 100%;
			height: auto;
		}
	</style>
	<?php
} );